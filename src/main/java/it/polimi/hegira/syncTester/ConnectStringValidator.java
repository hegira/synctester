package it.polimi.hegira.syncTester;

import org.apache.commons.validator.routines.InetAddressValidator;
import org.apache.commons.validator.routines.UrlValidator;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

/**
 * @author Marco Scavuzzo
 *
 */
public class ConnectStringValidator implements IParameterValidator {

	@Override
	public void validate(String name, String value) throws ParameterException {
		if(value.contains(":")){
			int ipStart = 0, ipEnd = value.indexOf(":")-1;
			int portStart = value.indexOf(":")+1, portEnd = value.length();
			String ip = value.substring(ipStart, ipEnd+1);
			String port = value.substring(portStart, portEnd);
			String[] schemes = {"http","https"};
			UrlValidator urlValidator = new UrlValidator(schemes);
			
			
			boolean isIp = ip.equals("localhost") || 
					InetAddressValidator.getInstance().isValid(ip) ||
					urlValidator.isValid(ip) ?
					true : false;
			boolean isPort;
			try{
				int portInt = Integer.parseInt(port);
				if(portInt>0)
					isPort=true;
				else
					isPort=false;
			}catch (NumberFormatException e){
				isPort=false;	
			}
			
			if(!isIp && !isPort)
				throw new ParameterException("Parameter " + name + 
						" should be of the form <ip>:<port>");
		}else{
			throw new ParameterException("Parameter " + name + 
					" should be of the form <ip>:<port>");
		}
	}

}
