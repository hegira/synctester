package it.polimi.hegira.syncTester.queries;

import it.polimi.hegira.kafkaProducer.productionLayer.KafkaMetamodelProducer;
import it.polimi.hegira.syncTester.eventSources.MigratedVdpSource;
import it.polimi.hegira.syncTester.eventSources.ScheduledStopSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class QueriesIssuerController {
	private static final transient Logger log = LoggerFactory.getLogger(QueriesIssuerController.class);
	
	private QueriesIssuerRunnable runnable;
	private List<Thread> runnableThreads;
	
	public QueriesIssuerController(){
		this(1, null, null, 0, null, false, 0, null, null,null);
	}
	
	public QueriesIssuerController(int threadNo, String kafkaConnectString, String topic,
			int vdpSize, HashMap<String, Integer> vdpsTablesList,
			boolean askForDMQsResponse,
			int wsPort,
			String dConnectString,
			Long seed,
			Integer groupOps){
		KafkaMetamodelProducer kafkaMetamodelProducer = null;
		if(kafkaConnectString != null && kafkaConnectString.contains(":") && 
				topic != null && !topic.isEmpty()) {
			if(askForDMQsResponse){
				if(wsPort<1024 || wsPort>65535 || dConnectString==null)
					throw new IllegalArgumentException("Wrong initialization parameters supplied!");
				try {
					kafkaMetamodelProducer = new KafkaMetamodelProducer(kafkaConnectString, topic, null, wsPort, dConnectString);
				} catch (Exception e) {
					log.error("Error creating the Kafka Metamodel Producer",e);
				}
			}else{
				kafkaMetamodelProducer = new KafkaMetamodelProducer(kafkaConnectString, topic);
			}
		}else{
			log.error("{} - Kafka connection string shouldn't be null and should be properly formatted (<ip>:<port>). "
					+ "The topic cannot be null.",
				Thread.currentThread().getName());
		}
		
		runnable = new QueriesIssuerRunnable(vdpSize, vdpsTablesList, kafkaMetamodelProducer, askForDMQsResponse, seed, groupOps);
		MigratedVdpSource.getInstance().addMigratedVdpListener(runnable);
		ScheduledStopSource.getInstance().addScheduledStopListener(runnable);
		
		threadNo = threadNo<=1 ? 1 : threadNo;
		runnableThreads = new ArrayList<Thread>(threadNo);
		int i;
		for(i=0; i<threadNo && runnable!=null; i++){
			runnableThreads.add(new Thread(runnable));
		}
		log.debug("{} - Added {} QueriesIssuerRunnable Threads",
				Thread.currentThread().getName(),
				i);
	}
	
	public void startThreads(){
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {}
		log.debug("{} - Starting QueriesIssuer Threads.",
				Thread.currentThread().getName());
		for(Thread t : runnableThreads){
			t.start();
			log.debug("{} - Starting Thread {}-{}.",
					Thread.currentThread().getName(),
					t.getName(),
					t.getId());
		}
	}
	
	public void stopThreads(){
		log.debug("{} - Stopping QueriesIssuer Threads.",
				Thread.currentThread().getName());
		if(runnable!=null)
			runnable.stopRunning();
	}
	
	public void waitThreadsTermination(){
		log.debug("{} - Waiting QueriesIssuer Threads Termination.",
				Thread.currentThread().getName());
	}
	
	public void setEntitiesPerVDPperc(float entitiesPerVDPperc) {
		if(entitiesPerVDPperc > 0 && entitiesPerVDPperc <= 100 && runnable!=null)
			runnable.setEntitiesPerVDPperc(entitiesPerVDPperc);
	}
	
	public void setVdpsPerTableperc(float vdpsPerTableperc) {
		if(vdpsPerTableperc > 0 && vdpsPerTableperc <= 100 && runnable!=null)
			runnable.setVdpsPerTableperc(vdpsPerTableperc);
	}

	public void setIntraQueriesPause(long intraQueriesPause) {
		if(intraQueriesPause>=-1 && runnable!=null)
			runnable.setIntraQueriesPause(intraQueriesPause);
	}
	
	public void setGenerateTweets(boolean generateTweets){
		runnable.setGenerateTweets(generateTweets);
	}
	
	public void setInterQuerySetsPause(long interQuerySetsPause){
		runnable.setInterQuerySetsPause(interQuerySetsPause);
	}
	
	public void setDebug(boolean debug){
		runnable.setDebug(debug);
	}
	
	public void setDMQsWaitTime(long waitTime){
		if(runnable!=null)
			runnable.setDMQsWaitTime(waitTime);
	}
	
	public void setDMQsWaitTimeTimeUnit(TimeUnit unit){
		if(runnable!=null)
			runnable.setDMQsWaitTimeTimeUnit(unit);
	}
	
	public void allowDeletes(boolean deletes){
		if(runnable!=null)
			runnable.setAllowDeletes(deletes);
	}
	
	public void setOutSnapshot(boolean outSnapshot){
		if(runnable!=null)
			runnable.setOutSnapshot(outSnapshot);
	}
	
	public void setInsideSnapshot(boolean insideSnapshot){
		if(runnable!=null)
			runnable.setInsideSnapshot(insideSnapshot);
	}
}
