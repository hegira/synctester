package it.polimi.hegira.syncTester.queries;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import it.polimi.hegira.hegira_metamodel.Column;
import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.hegira_metamodel.sync.MsgWrapper;
import it.polimi.hegira.hegira_metamodel.sync.Operations;
import it.polimi.hegira.kafkaProducer.productionLayer.KafkaMetamodelProducer;
import it.polimi.hegira.kafkaProducer.productionLayer.Transaction;
import it.polimi.hegira.kafkaProducer.ws.OperationResponse;
import it.polimi.hegira.syncTester.eventSources.DMQsResponseSource;
import it.polimi.hegira.syncTester.events.MigratedVdpEvent;
import it.polimi.hegira.syncTester.events.MigratedVdpListener;
import it.polimi.hegira.syncTester.events.ScheduledStopEvent;
import it.polimi.hegira.syncTester.events.ScheduledStopListener;
import it.polimi.hegira.syncTester.util.DefaultSerializer;
import it.polimi.hegira.syncTester.util.Stats;
import it.polimi.hegira.vdp.VdpUtils;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.commons.math3.util.Pair;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class QueriesIssuerRunnable 
  implements Runnable, MigratedVdpListener, ScheduledStopListener {
	private static final transient Logger log = LoggerFactory.getLogger(QueriesIssuerRunnable.class);
	private boolean stopThread = false, allMigrated = false;
	private boolean debug = false;
	
	private CopyOnWriteArrayList<String> migratedTablesList;
	private int vdpSize;
	private HashMap<String, Integer> vdpsTablesList;
	
	//The percentage of entities to generate per each VDP w.r.t. the VDPs' size
	private float entitiesPerVDPperc = 10.0f; 
	//The percentage of VDPs to consider to generate the entities w.r.t. the total number of VDPs per table
	private float vdpsPerTableperc = 30.0f;
	
	private KafkaMetamodelProducer commitlog;
	private long intraQueriesPause = 0;
	private long lastQueryTime;
	
	private RandomDataGenerator generator;
	private boolean generateTweets = false;
	private long interQuerySetsPause = 0;
	
	private boolean askForDMQsResponse;
	private long DMQsWaitTime;
	private TimeUnit DMQsWaitTimeTimeUnit;
	
	private Long seed;
	private Random rnd;
	
	private boolean allowDeletes=false;
	private boolean outSnapshot=false;
	private boolean insideSnapshot=true;
	private boolean allowDuplicatesOutSnpsht=false;
	
	private Transaction<MsgWrapper> nextTx;
	private Boolean nextTx_lock=false;
	private Integer groupOps;
	private TSerializer thriftSerializer;
	private HashMap<Integer,Object> duplicates;
	
	public QueriesIssuerRunnable() {
		migratedTablesList = new CopyOnWriteArrayList<String>();
		lastQueryTime = System.currentTimeMillis();
		generator = new RandomDataGenerator();
		rnd = new Random();
		if(!allowDuplicatesOutSnpsht)
			duplicates=new HashMap<Integer,Object>(3000);
	}
	
	public QueriesIssuerRunnable(int vdpSize, HashMap<String, Integer> vdpsTablesList,
			KafkaMetamodelProducer commitlog, boolean askForDMQsResponse, Long seed, final Integer groupOps) {
		migratedTablesList = new CopyOnWriteArrayList<String>();
		this.vdpSize = vdpSize;
		this.vdpsTablesList = new HashMap<String,Integer>(vdpsTablesList);
		this.commitlog = commitlog;
		lastQueryTime = System.currentTimeMillis();
		generator = new RandomDataGenerator();
		this.askForDMQsResponse =  askForDMQsResponse;
		if(seed!=null && seed.longValue()>0L) {
			this.seed = seed;
			rnd = new Random(seed);
		} else {
			rnd = new Random();
			log.warn("{} - A {} seed was inserted. No seed will be considered for the random generation.",
					Thread.currentThread().getName(),
					seed==null ? "NULL" : "Non-positive");
		}
		
		if(groupOps!=null && groupOps>1){
			this.groupOps=groupOps;
			thriftSerializer = new TSerializer(new TBinaryProtocol.Factory());
		}
		
		if(!allowDuplicatesOutSnpsht)
			duplicates=new HashMap<Integer,Object>(3000);
	}
	
	@Override
	public void run() {
		if(!isClassInitialized()){
			log.error("{} - Initialization check failed",
					Thread.currentThread().getName());
			return;
		}
		while(!stopThread && !allMigrated){
			generateEntities(outSnapshot, insideSnapshot);
			//relax a little bit
			pause(interQuerySetsPause);
		}
		
		if(allMigrated){
			log.debug("{} - Migration has finished. Generating some more DMQs and exit.\n\n",
					Thread.currentThread().getName());
			//Produce some other entities after migration has finished and exit
			generateEntities(true, false);
		}
		
		//if we have some spare ops
		if(groupOps!=null){
			synchronized(nextTx_lock){
				if(nextTx!=null){
					executeLastTx();
				}
			}
		}
	}
	
	private boolean isClassInitialized(){
		boolean flag = true;
		if(vdpSize<=0 || vdpsTablesList==null){
			log.error("{} - The class should be properly be initialized "
					+ "by specifing both the VDPs' size and the hashmap containing the tables and last VDP.",
				Thread.currentThread().getName());
			flag = false;
		} 
		if(commitlog==null){
			log.error("{} - The class should be properly be initialized "
					+ "by setting the proper KafkaMetamodelProducer object.",
				Thread.currentThread().getName());
			flag = false;
		}
		return flag;
	}
	
	private void pause(long max){
		if(max<=0) return;
		int min = 500;
		max = min >= max ? 500 : max;
		Random r = new Random();
		long rand = (long) ((max - min)*r.nextDouble() + min);
		if(debug)
			log.debug("{} - Generating inter query sets pause {} ms long. (min: {}, max: {}) ",
						Thread.currentThread().getName(),
						rand,min,max);
		try {
			Thread.sleep(rand);
		} catch (InterruptedException e) {}
	}
	
	/**
	 * @param interQuerySetsPause the interQuerySetsPause to set
	 */
	public synchronized void setInterQuerySetsPause(long interQuerySetsPause) {
		this.interQuerySetsPause = interQuerySetsPause;
	}

	private void applyIntraQueriesPause(){
		if(this.intraQueriesPause<1) return;
		
		long newQueryTime = System.currentTimeMillis();
		//how much time passed since last query?
		long elapsedTime = newQueryTime - lastQueryTime;
		lastQueryTime = newQueryTime;
		
		if(elapsedTime<intraQueriesPause){
			try {
				if(debug){
					log.debug("{} - Applying intra queries pause of duration {} ms.",
							Thread.currentThread().getName(),
							intraQueriesPause-elapsedTime);
				}
				Thread.sleep(intraQueriesPause-elapsedTime);
			} catch (InterruptedException e) {}
		}
	}
	
	private void generateEntities(boolean allowOutOfSnapshot, boolean allowInsideSnapshot){
		final int additionalVDPs = 10;
		Set<String> tables = vdpsTablesList.keySet();
		for(String table : tables){
			Integer totalVDPs = vdpsTablesList.get(table);
			totalVDPs = allowOutOfSnapshot ? totalVDPs+additionalVDPs : totalVDPs;
			//generating random vdps' ids to consider for this table
			int consideredVDPsNo = (int) Math.floor(totalVDPs*(vdpsPerTableperc/100));
			/*List<Integer> vdpIds = allowOutOfSnapshot ? 
					randIntList((int) (totalVDPs*0.5),totalVDPs,consideredVDPsNo) :
					randIntList(0,totalVDPs,consideredVDPsNo);*/
			List<Integer> vdpIds;
			
			if(allowInsideSnapshot && allowOutOfSnapshot){
				vdpIds = randIntList((int) (totalVDPs*0.5),totalVDPs,consideredVDPsNo);
			}else if(allowInsideSnapshot && !allowOutOfSnapshot){
				vdpIds = randIntList(0,totalVDPs,consideredVDPsNo);
			}else if(!allowInsideSnapshot && allowOutOfSnapshot){
				vdpIds = randIntList(vdpsTablesList.get(table)+1,totalVDPs,consideredVDPsNo);
			}else{
				//!allowInsideSnapshot && !allowOutOfSnapshot
				vdpIds = new ArrayList<Integer>();
				log.warn("I cannot generate operations since neither operations inside the snapshot "+
						"nor outside are allowed!");
			}
			
			List<Integer> entitiesIds = null;
			for(Integer vdpId : vdpIds){
				//generating random entities' ids per each considered vdp
				int consideredEntitiesNo = (int) Math.floor(Math.pow(10, vdpSize)*(entitiesPerVDPperc/100));
				//we're possibly going out of snapshot if the last vdp is considered
				//and if the lastSeqNr<vdpSize*totalVDPs, but it's perfectly fine!
				int[] vdpExtremes = VdpUtils.getVdpExtremes(vdpId, ((int) Math.pow(10, vdpSize)*totalVDPs),
						vdpSize);
				if(debug){
					log.debug("{} - \nGetting {} random entities from VDP {} \n(lastSeqNr: {}, vdpSize: {}, "
							+ "entitiesPerVDPperc: {}, consideredVDPsNo:{}) in range [{},{}]{}\n",
						Thread.currentThread().getName(),
						consideredEntitiesNo,vdpId, Math.pow(10, vdpSize)*totalVDPs, 
						Math.pow(10, vdpSize), entitiesPerVDPperc, 
						consideredVDPsNo,vdpExtremes[0],vdpExtremes[1],
						this.seed!=null && this.seed>0L ? 
								", with seed "+this.seed : "");
					
				}
				entitiesIds = randIntList(vdpExtremes[0],vdpExtremes[1],consideredEntitiesNo);
				for(Integer entityId : entitiesIds){
					//Skipping duplicated operations if only generating out-of-snapshot operations
					if(allowOutOfSnapshot && !insideSnapshot){
						if(!allowDuplicatesOutSnpsht && duplicates!=null){
							if(duplicates.containsKey(entityId))
								continue;
							else
								duplicates.put(entityId,null);
						}
					}
					
					try{
						if(debug){
							log.debug("{} - Generating entity with id:{}, of type: {}",
									Thread.currentThread().getName(),
									entityId,
									generateTweets ? "Tweet" : "KV entity");
						}
						//create a Metamodel entity
						Metamodel mentity = null;
						if(generateTweets)
							mentity = createTweet(entityId, table);
						else
							mentity = createEntity(entityId, table);
						
						//Execute DMQ
						boolean performed = performOperation(mentity, allowOutOfSnapshot);
						if(performed)
							applyIntraQueriesPause();
					}catch(IOException e){
						log.error("{} - Serialization Exception. Stack Trace:\n {}",
								Thread.currentThread().getName(),
								e);
					}
				}
			}
				
			if(debug)
				log.debug("{} - Generated {} DMQs, over {} VDPs for table {} ",
						Thread.currentThread().getName(),
						entitiesIds != null ? entitiesIds.size() : 0,
						vdpIds != null ? vdpIds.size() : 0,
						table);
		}
		
	}
	
	private void executeLastTx() {
		int txSize = nextTx.size();
		int txId = nextTx.getTransactionId();
		ArrayList<Future<OperationResponse>> responsesList = commitlog.executeTransaction(nextTx, askForDMQsResponse);
		Stats.getInstance().incrementTx();
		nextTx=null;
		handleOperationResponse(responsesList);
		
		if(debug){
			if(groupOps!=null && nextTx==null){
				
				log.debug("{} - Submitted LAST TX {} containing {} txs.",
						Thread.currentThread().getName(), txId, txSize);
			}
		}
	}

	private boolean performOperation(Metamodel mentity, boolean allowOutOfSnapshot){
		Operations operation = Operations.randomOperation(allowDeletes);
		boolean performed = false;
		Pair<Boolean,ArrayList<Future<OperationResponse>>> txReturnValue;
		
		ArrayList<Future<OperationResponse>> responsesList = new ArrayList<Future<OperationResponse>>();
		try {
			switch(operation){
				case INSERT:		
					if(allowOutOfSnapshot && isOutOfSnapshot(mentity)){
						if(groupOps==null){
							Future<OperationResponse> response = commitlog.insert(mentity, askForDMQsResponse);
							responsesList.add(response);
							updateStats(mentity, operation);
							performed = true;
						}else{
							txReturnValue = handleTransactions(mentity, operation, responsesList);
							performed=txReturnValue.getFirst();
							responsesList=txReturnValue.getSecond();
						}
					}else{
						performed = false;
						if(debug)
							log.debug("{} - INSERTion of entityId: {} not allowed: allowOutOfSnapshot {}, "
									+ "isOutOfSnapshot {}",
									Thread.currentThread().getName(),
									mentity.getRowKey(),
									allowOutOfSnapshot, isOutOfSnapshot(mentity));
					}
					break;
				case UPDATE:
					if(groupOps==null){
						Future<OperationResponse>  response = commitlog.update(mentity, askForDMQsResponse);
						responsesList.add(response);
						updateStats(mentity, operation);
						performed = true;
					}else{
						txReturnValue = handleTransactions(mentity, operation, responsesList);
						performed=txReturnValue.getFirst();
						responsesList=txReturnValue.getSecond();
					}
					break;
				case DELETE:
					if(groupOps==null){
						Future<OperationResponse> response = commitlog.delete(mentity, askForDMQsResponse);
						responsesList.add(response);
						updateStats(mentity, operation);
						performed = true;
					}else{
						txReturnValue = handleTransactions(mentity, operation, responsesList);
						performed=txReturnValue.getFirst();
						responsesList=txReturnValue.getSecond();
					}
					break;
				default:
					log.error("{} - Some other operation ({}) on entityId: {} has been issued!",
							Thread.currentThread().getName(),
							operation,
							mentity.getRowKey());
					performed = false;
					break;
			}
		} catch (TException e) {
			log.error("{} - Error serializing entity while executing operation: {} on entityId: {}.\n"
					+ "Stack trace:\n",
					Thread.currentThread().getName(),
					operation,
					mentity.getRowKey(),
					e);
		} catch(Exception e){
			log.error("{} - Error executing operation: {} on entityId: {}.\n"
					+ "Stack trace:\n",
					Thread.currentThread().getName(),
					operation,
					mentity.getRowKey(),
					e);
		}
		
		handleOperationResponse(responsesList);
		return performed;
	}
	
	private Pair<Boolean,ArrayList<Future<OperationResponse>>> handleTransactions(Metamodel mentity, Operations operation, List<Future<OperationResponse>> responsesList) throws TException{
		
		synchronized(nextTx_lock){
			if(nextTx==null)
				nextTx = new Transaction<MsgWrapper>(randIntList(1, Integer.MAX_VALUE, 1).get(0));
			
			MsgWrapper msg = new MsgWrapper(mentity.rowKey,operation);
			msg.setContent(thriftSerializer.serialize(mentity));
			msg.setTimestamp(System.currentTimeMillis());
	
			nextTx.addOperation(msg);
			updateStats(mentity, operation);
			
			int txSize = nextTx.size();
			if(txSize>=groupOps){
				int txId = nextTx.getTransactionId();
				responsesList = commitlog.executeTransaction(nextTx, askForDMQsResponse);
				nextTx=null;
				Stats.getInstance().incrementTx();
				if(debug){
					log.debug("{} - Submitted TX {}, size: {}",
							Thread.currentThread().getName(),txId,txSize);
				}
			}
		}
		return new Pair<Boolean,ArrayList<Future<OperationResponse>>>(true, (ArrayList<Future<OperationResponse>>) responsesList);
	}
	
	private void updateStats(Metamodel mentity, Operations op){
		switch(op){
			case INSERT:
				Stats.getInstance().incrementInserts(isOutOfSnapshot(mentity));
				break;
			case UPDATE:
				Stats.getInstance().incrementUpdates(isOutOfSnapshot(mentity));
				break;
				
			case DELETE:
				Stats.getInstance().incrementDeletes(isOutOfSnapshot(mentity));
				break;
		}
		
		if(debug){
			log.debug("{} - Submitted {} with entityId: {}.",
					Thread.currentThread().getName(), op.name(),
					mentity.getRowKey());
		}
	}
	
	private void handleOperationResponse(ArrayList<Future<OperationResponse>> responsesList){
		if(!askForDMQsResponse || responsesList==null){
			return;
		}
		
		for(Future<OperationResponse> response : responsesList)
			DMQsResponseSource
				.getInstance()
				.notifyDMQsResponse(DMQsWaitTime, DMQsWaitTimeTimeUnit, response);
	}
	
	private Metamodel createEntity(Integer entityId, String tableName) throws IOException{
		final Column col = new Column();
		col.setColumnName("columnName");
		//col.setColumnValue(RandomStringUtils.random(20, true, true).getBytes());
		col.setColumnValue(
				it.polimi.hegira.syncTester.util.DefaultSerializer.serialize(RandomStringUtils.random(20, true, true)+
						"-SYNC"));
		col.setColumnValueType(String.class.getSimpleName());
		col.setIndexable(false);
		ArrayList<Column> cols = new ArrayList<Column>();
		cols.add(col);
		Map<String, List<Column>> kindcols = new HashMap<String, List<Column>>();
		kindcols.put(tableName,
				cols);
		Metamodel mm = new Metamodel();
		mm.setPartitionGroup(tableName+"#"+entityId);
		mm.setRowKey(""+entityId);
		mm.setColumns(kindcols);
		ArrayList<String> cfs = new ArrayList<String>();
		cfs.add(tableName);
		mm.setColumnFamilies(cfs);
		return mm;
	}
	
	protected static String millsToDateString(long time){
		Date date = new Date(time);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
	
	private Metamodel createTweet(Integer entityId, String tableName) throws IOException {
		Metamodel mm = new Metamodel();
		mm.setRowKey(""+entityId);
		mm.setPartitionGroup(tableName+"#"+entityId);
		
		createColumns(mm, tableName);
		
		//add column families
		ArrayList<String> cfs = new ArrayList<String>();
		cfs.add(tableName);
		mm.setColumnFamilies(cfs);
		return mm;
	}
	
	private void createColumns(Metamodel metamodel, String tableName) throws IOException{
		ArrayList<Column> cols = new ArrayList<Column>();
		
		//accessLevel
		Column al = createColumn("accessLevel", 0);
		cols.add(al);
		
		//currentUserRetweetId -1
		Column cur = createColumn("currentUserRetweetId", -1);
		cols.add(cur);
		
		//favoriteCount -1
		Column fc = createColumn("favoriteCount", -1);
		cols.add(fc);
		
		//inReplyToStatusId -1
		Column irts = createColumn("inReplyToStatusId", -1);
		cols.add(irts);
		
		//retweetCount 0
		Column rc = createColumn("retweetCount", 0);
		cols.add(rc);
		
		//source <a href="http://twitter.com/download/android" rel="nofollow">Twitter for Android</a>
		Column source = 
				createColumn("source", "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>");
		cols.add(source);
		
		//text Im going to sleep. Maybe 
		Column txt = createColumn("text", "Im going to sleep. Maybe");
		cols.add(txt);
		
		//tweet_id Random long
		Column tid = createColumn("tweet_id", generator.nextLong(274769807155535893L, 999999907155535873L));
		cols.add(tid);
		
		//urlEntities []
		Column ue = createColumn("urlEntities", "[]");
		cols.add(ue);
		
		//user getUserField()
		Column user = createColumn("user", getUserField());
		cols.add(user);
		
		//hashtags []
		Column hts = createColumn("hashtags", "[]");
		cols.add(hts);
		
		//createdAt millsToDateString(System.currentTimeMillis())
		createColumn("createdAt", new Date(System.currentTimeMillis()));
		
		Map<String, List<Column>> kindcols = new HashMap<String, List<Column>>();
		kindcols.put(tableName, cols);
		metamodel.setColumns(kindcols);
	}
	
	private Column createColumn(String name, Object value) throws IOException {
		final Column col = new Column();
		col.setColumnName(name);
		col.setColumnValue(DefaultSerializer.serialize(value));
		col.setColumnValueType(value.getClass().getSimpleName());
		col.setIndexable(false);
		return col;
	}
	
	private static String getUserField(){
		return " {\"biggerProfileImageURLHttps\":\"https://si0.twimg.com/profile_images/2903460413/36e60787ec0ce62181f729339455ede0_bigger.jpeg\",\"profileImageURL\":\"http://a0.twimg.com/profile_images/2903460413/36e60787ec0ce62181f729339455ede0_normal.jpeg\",\"friendsCount\":370,\"profileBackgroundColor\":\"1A1B1F\",\"profileUseBackgroundImage\":true,\"location\":\"Texas Bityyyyyyyyyyyyy, Tx ( :\",\"followRequestSent\":false,\"biggerProfileImageURL\":\"http://a0.twimg.com/profile_images/2903460413/36e60787ec0ce62181f729339455ede0_bigger.jpeg\",\"favouritesCount\":132,\"screenName\":\"YOmanlikeit\",\"timeZone\":\"Central Time (US & Canada)\",\"URLEntity\":{\"text\":\"\",\"expandedURL\":\"\",\"start\":0,\"displayURL\":\"\",\"URL\":\"\",\"end\":0},\"profileSidebarBorderColor\":\"181A1E\",\"lang\":\"en\",\"id\":338859020,\"miniProfileImageURL\":\"http://a0.twimg.com/profile_images/2903460413/36e60787ec0ce62181f729339455ede0_mini.jpeg\",\"profileSidebarFillColor\":\"252429\",\"protected\":false,\"createdAt\":\"Wed Jul 20 06:08:31 UTC 2011\",\"verified\":false,\"description\":\"BlE$$ED! \n\nIf you see smoke, that's 0UR section. If you see ho's, that's our SELECTi0N.. At the strip club during a RECE$$i0N!\n \n. Freeeeeee Phil ; R I P MONSTA\",\"name\":\"Charlene Baltimore♥\",\"originalProfileImageURL\":\"http://a0.twimg.com/profile_images/2903460413/36e60787ec0ce62181f729339455ede0.jpeg\",\"contributorsEnabled\":false,\"profileBackgroundImageURL\":\"http://a0.twimg.com/profile_background_images/640909898/4q5yb5ffn0yqb7yekftk.jpeg\",\"profileBackgroundImageUrlHttps\":\"https://si0.twimg.com/profile_background_images/640909898/4q5yb5ffn0yqb7yekftk.jpeg\",\"profileImageURLHttps\":\"https://si0.twimg.com/profile_images/2903460413/36e60787ec0ce62181f729339455ede0_normal.jpeg\",\"miniProfileImageURLHttps\":\"https://si0.twimg.com/profile_images/2903460413/36e60787ec0ce62181f729339455ede0_mini.jpeg\",\"listedCount\":1,\"geoEnabled\":false,\"originalProfileImageURLHttps\":\"https://si0.twimg.com/profile_images/2903460413/36e60787ec0ce62181f729339455ede0.jpeg\",\"accessLevel\":0,\"profileLinkColor\":\"711D9E\",\"translator\":false,\"descriptionURLEntities\":[],\"showAllInlineMedia\":false,\"profileBackgroundTiled\":true,\"followersCount\":536,\"utcOffset\":-21600,\"profileTextColor\":\"E82767\",\"statusesCount\":46936}";
	}
	
	/**
	 * Generates a {@link List} containing random {@link Integer} numbers within the range [min, max].
	 * @param min The minimum
	 * @param max The maximum
	 * @param elemNo The number of random elements to generate
	 * @return
	 */
	private List<Integer> randIntList(int min, int max, int elemNo){
		if(debug)
			log.debug("{} - Received parameters min: {}, max: {}, elemNo: {} ",
					Thread.currentThread().getName(),
					min,max,elemNo);
		if (min > max || elemNo<1) {
			throw new IllegalArgumentException("max must be greater than min and elemNo>0");
		}
		ArrayList<Integer> list = new ArrayList<Integer>(elemNo);
		for(int i=0; i<elemNo;i++){
			int rand = rnd.nextInt((max - min) + 1) + min;
			list.add(rand);
		}
		return list;
	}

	public void stopRunning(){
		stopThread = true;
		log.info(Thread.currentThread().getName()+
				" - Stopping thread");
	}
	
	/**
	 * Checks if an entity is out of the snapshot
	 * @param mentity The entity to check for.
	 * @return true if out of snapshot, false if inside the snapshot
	 */
	private boolean isOutOfSnapshot(Metamodel mentity){
		if(mentity==null) return true;
		int key = Integer.parseInt(mentity.getRowKey());
		int mmvdp = VdpUtils.getVDP(key, vdpSize);
		String tableName = mentity.getColumnFamilies().get(0);
		if(tableName!=null){
			Integer lastVDP = vdpsTablesList.get(tableName);
			return lastVDP!=null && mmvdp > lastVDP ? true : false;
		}
		return false;
	}

	@Override
	public void onVdpMigrated(MigratedVdpEvent event) {
		if(event!=null){
			migratedTablesList.addIfAbsent(event.getTableName());
			log.debug("{} - Received VDP migrated Event on table {} ",
				Thread.currentThread().getName(),
				event.getTableName());
		}
		//check if all tables have been migrated
		if(vdpsTablesList!=null && !vdpsTablesList.isEmpty()){
			if(migratedTablesList.containsAll(vdpsTablesList.keySet())){
				allMigrated=true;
				log.info(Thread.currentThread().getName()+
						" - All tables have been migrated!");
			}
		}
	}

	/**
	 * @return the vdpSize
	 */
	public int getVdpSize() {
		return vdpSize;
	}

	/**
	 * @param vdpSize the vdpSize to set
	 */
	public synchronized void setVdpSize(int vdpSize) {
		this.vdpSize = vdpSize;
	}

	/**
	 * @return the vdpsTablesList
	 */
	public HashMap<String, Integer> getVdpsTablesList() {
		return vdpsTablesList;
	}

	/**
	 * @param vdpsTablesList the vdpsTablesList to set
	 */
	public synchronized void setVdpsTablesList(HashMap<String, Integer> vdpsTablesList) {
		this.vdpsTablesList = vdpsTablesList;
	}

	/**
	 * Gets the percentage of entities to generate per each VDP w.r.t. the VDPs' size
	 * @return the entitiesPerVDPperc
	 */
	public float getEntitiesPerVDPperc() {
		return entitiesPerVDPperc;
	}

	/**
	 * Sets the percentage of entities to generate per each VDP w.r.t. the VDPs' size
	 * @param entitiesPerVDPperc the entitiesPerVDPperc to set
	 */
	public synchronized void setEntitiesPerVDPperc(float entitiesPerVDPperc) {
		this.entitiesPerVDPperc = entitiesPerVDPperc;
	}

	/**
	 * Gets the percentage of VDPs to consider to generate the entities w.r.t. the total number of VDPs per table
	 * @return the vdpsPerTableperc
	 */
	public float getVdpsPerTableperc() {
		return vdpsPerTableperc;
	}

	/**
	 * Sets the percentage of VDPs to consider to generate the entities w.r.t. the total number of VDPs per table
	 * @param vdpsPerTableperc the vdpsPerTableperc to set
	 */
	public synchronized void setVdpsPerTableperc(float vdpsPerTableperc) {
		this.vdpsPerTableperc = vdpsPerTableperc;
	}

	/**
	 * @param commitlog the commitlog to set
	 */
	public synchronized void setCommitlog(KafkaMetamodelProducer commitlog) {
		this.commitlog = commitlog;
	}

	public void setIntraQueriesPause(long intraQueriesPause) {
		this.intraQueriesPause = intraQueriesPause;
		
	}
	
	public void setGenerateTweets(boolean generateTweets){
		this.generateTweets = generateTweets;
	}
	
	public void setDebug(boolean debug){
		this.debug = debug;
	}

	@Override
	public void onScheduledStop(ScheduledStopEvent event) {
		log.info("{} - Stopping queries after {} {}.",
				Thread.currentThread().getName(),
				event.getDelay(), event.getUnit());
		stopRunning();
	}

	public long getDMQsWaitTime() {
		return DMQsWaitTime;
	}

	public void setDMQsWaitTime(long dMQsWaitTime) {
		DMQsWaitTime = dMQsWaitTime;
	}

	public TimeUnit getDMQsWaitTimeTimeUnit() {
		return DMQsWaitTimeTimeUnit;
	}

	public void setDMQsWaitTimeTimeUnit(TimeUnit dMQsWaitTimeTimeUnit) {
		DMQsWaitTimeTimeUnit = dMQsWaitTimeTimeUnit;
	}

	public boolean isAllowDeletes() {
		return allowDeletes;
	}

	public void setAllowDeletes(boolean allowDeletes) {
		this.allowDeletes = allowDeletes;
	}

	public boolean isOutSnapshot() {
		return outSnapshot;
	}

	public void setOutSnapshot(boolean outSnapshot) {
		this.outSnapshot = outSnapshot;
	}
	
	public void setInsideSnapshot(boolean insideSnapshot) {
		this.insideSnapshot = insideSnapshot;
	}
}