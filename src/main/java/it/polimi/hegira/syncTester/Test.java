package it.polimi.hegira.syncTester;

import java.util.StringTokenizer;

public class Test {

	public static void main(String[] args) {
		//String command = "-dbg -p 600 -sp 600 stop -d 2 DMQsResponse -l";
		String command = "-dbg -k 10.10.10.10:9191 -p 600 -sp 600 DMQsResponse -l";
		
		//String command = "-dbg -p 600 -sp 600";
		StringTokenizer st = new StringTokenizer(command);
		
		int tokensNum = st.countTokens();
		String[] newArgs = new String[tokensNum];
		
		for(int i=0; i<tokensNum && st.hasMoreTokens(); i++){
			newArgs[i]=st.nextToken();
		}
		
		EntryClass.main(newArgs);

	}

}
