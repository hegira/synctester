/**
 * 
 */
package it.polimi.hegira.syncTester.events;

import java.util.EventObject;
import java.util.concurrent.TimeUnit;

/**
 * @author Marco Scavuzzo
 *
 */
public class ScheduledStopEvent extends EventObject {
	
	private static final long serialVersionUID = 7369877358321760864L;
	private long delay;
	private TimeUnit unit;

	public ScheduledStopEvent(Object source, long delay, TimeUnit unit) {
		super(source);
		this.delay = delay;
		this.unit = unit;
	}

	/**
	 * @return the delay
	 */
	public synchronized long getDelay() {
		return delay;
	}

	/**
	 * @return the unit
	 */
	public synchronized TimeUnit getUnit() {
		return unit;
	}

}
