package it.polimi.hegira.syncTester.events;

import java.util.EventListener;

/**
 * @author Marco Scavuzzo
 *
 */
public interface MigratedVdpListener extends EventListener {
	
	public void onVdpMigrated(MigratedVdpEvent event);
}
