/**
 * 
 */
package it.polimi.hegira.syncTester.events;

import java.util.EventListener;

/**
 * @author Marco Scavuzzo
 *
 */
public interface ScheduledStopListener extends EventListener {
	public void onScheduledStop(ScheduledStopEvent event);
}
