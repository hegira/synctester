/**
 * 
 */
package it.polimi.hegira.syncTester.events;

import java.util.EventObject;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import it.polimi.hegira.kafkaProducer.ws.OperationResponse;

/**
 * @author Marco Scavuzzo
 *
 */
public class DMQresponseEvent extends EventObject {
	private long delay;
	private TimeUnit delayUnit;
	private Future<OperationResponse> response;
	
	public DMQresponseEvent(Object obj, long delay, TimeUnit unit, Future<OperationResponse> response) {
		super(obj);
		this.delay = delay;
		this.delayUnit = unit;
		this.response = response;
	}

	public long getDelay() {
		return delay;
	}

	public TimeUnit getDelayUnit() {
		return delayUnit;
	}

	public Future<OperationResponse> getResponse() {
		return response;
	}
}
