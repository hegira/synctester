/**
 * 
 */
package it.polimi.hegira.syncTester.events;

import java.util.EventObject;

/**
 * @author Marco Scavuzzo
 *
 */
public class MigratedVdpEvent extends EventObject {
	private String tableName;
	private Integer vdpId;
	private long timestamp;
	
	
	
	public MigratedVdpEvent(Object source, String tableName, Integer vdpId,
			long timestamp) {
		super(source);
		this.tableName = tableName;
		this.vdpId = vdpId;
		this.timestamp = timestamp;
	}



	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}



	/**
	 * @return the vdpId
	 */
	public Integer getVdpId() {
		return vdpId;
	}



	/**
	 * @return the timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}

	

}
