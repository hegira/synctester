/**
 * 
 */
package it.polimi.hegira.syncTester.events;

import java.util.EventListener;

/**
 * @author Marco Scavuzzo
 *
 */
public interface DMQsResponseListener extends EventListener {
	public void onDMQresponse(DMQresponseEvent event);
}
