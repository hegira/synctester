package it.polimi.hegira.syncTester;

import java.io.Console;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.polimi.hegira.syncTester.CLI.DMQsResponseCommand;
import it.polimi.hegira.syncTester.CLI.GroupOperations;
import it.polimi.hegira.syncTester.CLI.StopCommand;
import it.polimi.hegira.syncTester.coordination.VDPsCheckerController;
import it.polimi.hegira.syncTester.coordination.ZooKeeper;
import it.polimi.hegira.syncTester.dmqLogging.DMQsLogging;
import it.polimi.hegira.syncTester.eventSources.DMQsResponseSource;
import it.polimi.hegira.syncTester.exceptions.InitializationException;
import it.polimi.hegira.syncTester.queries.QueriesIssuerController;
import it.polimi.hegira.syncTester.schedulers.StopScheduler;
import it.polimi.hegira.syncTester.util.Stats;
import it.polimi.hegira.syncTester.util.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;

/**
 * @author Marco Scavuzzo
 *
 */
public class EntryClass {
	private static final transient Logger log = LoggerFactory.getLogger(EntryClass.class);
	private static boolean debug = true;
	
	private static HashMap<String, Integer> vdpsTables;
	
	private static VDPsCheckerController vdPsCheckerController;
	private static Integer groupOpSize;
	
	public static void main(String[] args) {
		System.out.println("Home path: "+System.getProperty("user.home")+File.separator+
			"hegira"+File.separator+
			"syncTester"+File.separator+"\n");
		if(debug)
			log.info(Util.title);
		
		CLI cli = new CLI();
		JCommander jc = new JCommander(cli);
		jc.setProgramName("Sync Tester");
		
		StopCommand sc = cli.new StopCommand();
		jc.addCommand(StopCommand.commandName, sc);
		
		DMQsResponseCommand dmqRc = cli.new DMQsResponseCommand();
		jc.addCommand(DMQsResponseCommand.commandName, dmqRc);
		boolean askForDMQsResponse=false;
		
		GroupOperations gOp = cli.new GroupOperations();
		jc.addCommand(GroupOperations.commandName, gOp);
		
		StopScheduler stopScheduler = null;
		
		try{
			
			Map<String, String[]> parsedCommands = getParsedCommands(args, jc);
			jc = new JCommander(cli, parsedCommands.get("pre"));
			if(cli.help){
				jc.usage();
				return;
			}
			
			if(parsedCommands.containsKey(StopCommand.commandName)){
				StopCommand newSc = cli.new StopCommand();
				JCommander jcSc = new JCommander(cli);
				jcSc.addCommand(StopCommand.commandName, newSc);
				jcSc.parse(parsedCommands.get(StopCommand.commandName));
				
				stopScheduler = new StopScheduler(newSc.stopDelay, newSc.parseTimeUnit(newSc.stopDelayUnit));
				stopScheduler.scheduleStop();
			}
			
			//adds listeners for DMQs responses
			if(parsedCommands.containsKey(DMQsResponseCommand.commandName)){
				DMQsResponseCommand newDmqRc = cli.new DMQsResponseCommand();
				JCommander jcDmqRc = new JCommander(cli);
				jcDmqRc.addCommand(DMQsResponseCommand.commandName, newDmqRc);
				jcDmqRc.parse(parsedCommands.get(DMQsResponseCommand.commandName));
				
				log.debug("User requested to receive DMQs responses");
				askForDMQsResponse = true;
				//if we are asked to log...
				if(newDmqRc.logging){
					log.debug("User requested the persistent log of received DMQs responses");
					//we register a DMQsLogging instance as a listener for DMQs responses
					DMQsLogging dmqsLogging = DMQsLogging.getInstance();
					DMQsResponseSource.getInstance().addDMQsResponseListener(dmqsLogging);
				}
			}
			
			if(parsedCommands.containsKey(GroupOperations.commandName)){
				GroupOperations newGop = cli.new GroupOperations();
				JCommander jcGop = new JCommander(cli);
				jcGop.addCommand(GroupOperations.commandName, newGop);
				jcGop.parse(parsedCommands.get(GroupOperations.commandName));
				
				log.debug("Generating operations in group of {} elements", newGop.groupSize);
				groupOpSize = new Integer(newGop.groupSize);
			}
			
		}catch(Exception e){
			log.error("",e);
			if(jc.getParsedCommand()!=null){
				jc.usage(jc.getParsedCommand());
			} else {
				jc.usage();
			}
			return;
		}
		
		//Key = tableName
		//Value = lastVDP
		vdpsTables = new HashMap<String, Integer>();
		
		try {
			//1. Retrieve the total VDP numbers in the main process
			@SuppressWarnings("static-access")
			ZooKeeper zk = ZooKeeper.init(cli.zkConnectString, cli.zkServerVersion).getInstance();
			boolean isPopulated = populateVdpsTables(zk);
			if(!isPopulated){
				log.error("Does Zookeeper contain the VDP statuses? Please check.");
				return;
			}
			
			//2. Create a thread that periodically checks for the last VDPs statuses and sends a notification
			vdPsCheckerController = new VDPsCheckerController(zk, vdpsTables);
			
			
			//3. Start issuing DMQs for all the tables and listen for migration conclusion
			//4. When the lastVDP changes, issue a limited number of DMQs and exit.
			QueriesIssuerController issuerController = new QueriesIssuerController(cli.queriesIssuers, 
					cli.kafkaConnectString, 
					cli.clTopic, 
					zk.getVDPsize(true), 
					vdpsTables,
					askForDMQsResponse,
					dmqRc.port,
					dmqRc.dConnectString,
					cli.seed,
					groupOpSize);
			issuerController.setEntitiesPerVDPperc(cli.entitiesPerVDPperc);
			issuerController.setVdpsPerTableperc(cli.vdpsPerTableperc);
			issuerController.setIntraQueriesPause(cli.intraQueriesPause);
			issuerController.setGenerateTweets(cli.generateTweets);
			issuerController.setInterQuerySetsPause(cli.interQuerySetsPause);
			issuerController.setDebug(cli.debug);
			issuerController.allowDeletes(cli.deletes);
			issuerController.setOutSnapshot(cli.outSnapshot);
			if(debug){
				log.debug("Got insideSnapshot flag = {}",cli.insideSnapshot);
			}
			issuerController.setInsideSnapshot(cli.insideSnapshot);
			
			if(askForDMQsResponse){
				issuerController.setDMQsWaitTime(dmqRc.waitTime);
				issuerController.setDMQsWaitTimeTimeUnit(dmqRc.parseTimeUnit(dmqRc.waitTimeUnit));
			}
			
			vdPsCheckerController.startThread();
			
			issuerController.startThreads();
			
			console(stopScheduler);
			
			exit();
		} catch (InitializationException e) {
			e.printStackTrace();
			return;
		}
		
	}
	
	private static Map<Integer, String> argsCommandsPositions(String[] args, JCommander jc){
		
		Map<String, JCommander> commands = jc.getCommands();
		Map<Integer, String> positions = new HashMap<Integer, String>(commands.size());
		
		for(int i=0; i<args.length; i++){
			if(commands.containsKey(args[i]))
				positions.put(i, args[i]);
		}
		return positions;
	}
	
	private static Map<String, String[]> getParsedCommands(String[] args, JCommander jc){
		if(args==null || jc==null)
			return null;
		Map<Integer, String> positions = argsCommandsPositions(args,jc);
		Map<String, String[]> jcommands = new HashMap<String, String[]>(positions.size());
		/*if(positions.isEmpty()){
			jc.parse(args);
			jcommands.put("NULL", args);
			return jcommands;
		}*/

		ArrayList<Integer> posList = new ArrayList<Integer>(positions.keySet());
		Collections.sort(posList);
		
		String[] preCommands = Arrays.copyOfRange(args, 0, posList.size()>0 ?posList.get(0) : args.length);
		jc.parse(preCommands);
		jcommands.put("pre", preCommands);
		
		for(int curr=0, next=curr+1;curr<posList.size();curr++,next++){
			int end=0;
			if(next==posList.size())
				end=args.length;
			else
				end=posList.get(next);
			
			String[] cmdAndArgs = Arrays.copyOfRange(args, posList.get(curr), end);
			String[] newArgs = concat(preCommands,cmdAndArgs);
			
			jcommands.put(positions.get(posList.get(curr)), newArgs);		
		}
		
		return jcommands;
	}
	
	
	private static <T> T[] concat(T[] first, T[] second) {
		  T[] result = Arrays.copyOf(first, first.length + second.length);
		  System.arraycopy(second, 0, result, first.length, second.length);
		  return result;
	}
	
	private static void console(StopScheduler stopScheduler){
		Console console = System.console();
        if (console == null){
        		log.error("No console active. Cannot accept interactive commands.");
            return;
        }
        boolean keepRunning = true;
        while (keepRunning)
        {       
            String name =  console.readLine("\n+-----------+"
            		+"\n|"
            		+ " COMMANDS: |\n"
            		+"+-----------+\n\n"
            		+ "\tstats: print statistics,\n"
            		+ "\tschedules: shows active schedules information,\n"
            		+ "\tdelete_schedule: deletes a stop schedule,\n"
            		+ "\texit: wait for program termination and quits,\n"
            		+ "\tforce_exit: terminates the program without waiting for proper termination.\n\n"
            		+ "Enter command: ");
           
            switch(name.toLowerCase()){
            		case "stats":
            			console.printf("%s", Stats.getInstance().toString());
            			break;
            		case "exit":
            			keepRunning = false;
            			break;
            		case "force_exit":
            			if(vdPsCheckerController!=null)
            				vdPsCheckerController.stopThread();
            			System.exit(-1);
            			return;
            		case "delete_schedule":
            			if(stopScheduler!=null){
            				String resp = console.readLine("Are you sure you want to delete the stop schedule? [y/n]");
            				if(resp!=null && resp.equals("y")){
            					boolean cancelled = stopScheduler.cancelScheduledStop();
            					if(cancelled)
            						console.printf("%s", "Stop Schedule was cancelled");
            					else
            						console.printf("%s", "Couldn't cancel the Stop Schedule");
            				}
            			}else{
            				console.printf("%s", "No Stop Schedule to cancel!!!");
            			}
            			break;
            		case "schedules":
            			if(stopScheduler!=null){
            				console.printf("%s", "Stop Schedule. \tRemaining time: "+stopScheduler.remainingTime());
            			}
            			break;
            		default:
            			break;
            }
        }
	}
	
	/**
	 * Retrieve the total VDP numbers 
	 * @param zk Initialized ZooKeeper instance
	 */
	private static boolean populateVdpsTables(ZooKeeper zk){
		List<String> tables = zk.getTablesList();
		boolean flag = vdpsTables!=null && tables!=null && tables.size()>0;
		if(tables!=null)
			for(String table : tables){
				int value = zk.getTableVDPsNo(table)-1;
				if(vdpsTables!=null){
					vdpsTables.put(table, value);
				}
			}
		return flag;
	}
	
	/**
	 * Waits until all threads have completed their job.
	 */
	private static void exit(){
		try {
			vdPsCheckerController.waitThreadTermination();
		} catch (InterruptedException e) {
			log.error("{} - ", 
					Thread.currentThread().getName(),
					e);
		}
		ZooKeeper.disconnect();
		log.info(Stats.getInstance().toString());
		Stats.getInstance().stopWriter();
	}

}
