package it.polimi.hegira.syncTester.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Marco Scavuzzo
 *
 */
public class Stats {
	private static final transient Logger log = LoggerFactory.getLogger(Stats.class);
	public Boolean runnableSleepMonitor = false;
	private static Stats instance = new Stats();
	private StatsObject statsObject = StatsObject.getInstance();
	private Gson gson;
	
	private String filePath = System.getProperty("user.home")+File.separator+
			"hegira"+File.separator+
			"syncTester"+File.separator+
			"stats.txt";
	private Thread statsWriterThread;
	private StatsWriter statsWriterRunnable;
	
	private static class StatsObject{
		public AtomicLong inserts;
		public AtomicLong updates;
		public AtomicLong deletes;
		
		public AtomicLong opOutOfSnapshot;
		public AtomicLong opInsideSnapshot;
		
		public AtomicLong transactions;
		
		private static transient StatsObject instance = new StatsObject();
		
		private StatsObject(){
			init();
		}
		
		private void init(){
			inserts = new AtomicLong(0);
			updates = new AtomicLong(0);
			deletes = new AtomicLong(0);
			opOutOfSnapshot = new AtomicLong(0);
			opInsideSnapshot = new AtomicLong(0);
			transactions = new AtomicLong(0);
		}
		
		public static StatsObject getInstance(){
			return instance;
		}
		
		public long getTotalOperations(){
			return inserts.longValue()+updates.longValue()+deletes.longValue();
		}
		
		@Override
		public String toString() {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Calendar cal = Calendar.getInstance();
			
			StringBuilder sb = new StringBuilder("Operations statistics at "+
					dateFormat.format(cal.getTime())+" :\n");
			sb.append("Inserts: "+inserts.longValue()+",\n");
			sb.append("Updates: "+updates.longValue()+",\n");
			sb.append("Deletes: "+deletes.longValue()+",\n");
			sb.append("Transactions: "+transactions.longValue()+",\n");
			sb.append("\nTotal Operations inside the snapshot: "+opInsideSnapshot.longValue()+",\n");
			sb.append("\nTotal Operations outside of the snapshot: "+opOutOfSnapshot.longValue()+",\n");
			sb.append("\nTotal Operations: "+getTotalOperations()+"\n");
			return sb.toString();
		}
	}

	private class StatsWriter implements Runnable {
		
		public boolean stopThread = false;
		public boolean mustSleep = false;
		
		@Override
		public void run() {
			while(!stopThread){
				persistStats();	
				waitOperation();
			}
		}
		
		private void persistStats(){
			if(filePath==null){
				stopThread = true;
				return;
			}
			BufferedWriter bw = null;
			try {
				File file = new File(filePath);
				if (!file.getParentFile().exists())
				    file.getParentFile().mkdirs();
				if (!file.exists())
				    file.createNewFile();
				bw = new BufferedWriter(new FileWriter(file,false));
				bw.write(getJson());
			} catch (IOException e) {
				log.error("{} - Error when trying to write file: {}",
						Thread.currentThread().getName(),
						filePath);
				stopThread=true;
				return;
			}finally{
				if(bw!=null){
					try {
						bw.close();
					} catch (IOException e) {
						log.error("{} - Failed to close the BufferedWriter:\n",
								Thread.currentThread().getName(),
								e);
					}
				}
			}
		}
		
		private void waitOperation(){
			if(stopThread)
				return;
			synchronized(runnableSleepMonitor){
				try {
//					log.debug("{} - Going to sleep",
//							Thread.currentThread().getName());
					while(mustSleep){
						runnableSleepMonitor.wait();
					}
					mustSleep = true;
				} catch (InterruptedException e) {
					log.error("{} - Cannot wait for next operation",
							Thread.currentThread().getName());
				}
			}
		}
	}
	
	private Stats(){
		this.gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting()
			     .setVersion(1.0).create();
		
		this.statsWriterRunnable = new StatsWriter();
		this.statsWriterThread = new Thread(statsWriterRunnable);
		this.statsWriterThread.start();
	}
	
	public static Stats getInstance(){
		return instance;
	}
	
	public long incrementInserts(boolean outOfsnapshot){
		incrementSnapshotOperations(outOfsnapshot);
		long updatedValue = statsObject.inserts.incrementAndGet();
		storeStats();
		return updatedValue;
	}
	
	public long getInserts(){
		return statsObject.inserts.longValue();
	}
	
	public long incrementUpdates(boolean outOfsnapshot){
		incrementSnapshotOperations(outOfsnapshot);
		long updatedValue = statsObject.updates.incrementAndGet();
		storeStats();
		return updatedValue;
	}
	
	public long getUpdates(){
		return statsObject.updates.longValue();
	}
	
	public long incrementDeletes(boolean outOfsnapshot){
		incrementSnapshotOperations(outOfsnapshot);
		long updatedValue = statsObject.deletes.incrementAndGet();
		storeStats();
		return updatedValue;
	}
	
	public long getDeletes(){
		return statsObject.deletes.longValue();
	}
	
	public long incrementTx(){
		long txValue = statsObject.transactions.incrementAndGet();
		storeStats();
		return txValue;
	}
	
	public long getTxs(){
		return statsObject.transactions.longValue();
	}
	
	private void incrementSnapshotOperations(boolean outOfsnapshot){
		if(!outOfsnapshot)
			statsObject.opInsideSnapshot.incrementAndGet();
		else
			statsObject.opOutOfSnapshot.incrementAndGet();
	}
	
	public long getTotalOperations(){
		return statsObject.getTotalOperations();
	}
	
	@Override
	public String toString() {
		return statsObject.toString();
	}
	
	public String getJson() {
		return gson.toJson(statsObject);
	}
	
	private void storeStats(){
		synchronized(runnableSleepMonitor){
			statsWriterRunnable.mustSleep = false;
			runnableSleepMonitor.notifyAll();
		}
		try {
			Thread.sleep(0,1);
		} catch (InterruptedException e) {
			log.error("{} - Waiting time between stores interrupted:\n",
					Thread.currentThread().getName(),
					e);
		}
	}
	
	public void stopWriter(){
		statsWriterRunnable.stopThread = true;
		
		synchronized(runnableSleepMonitor){
//			log.debug("{} - Awekening thread",
//					Thread.currentThread().getName());
			statsWriterRunnable.mustSleep = false;
			runnableSleepMonitor.notifyAll();
		}
		
		try {
			statsWriterThread.join();
		} catch (InterruptedException e) {
			log.error("{} - Cannot wait for WriterThread to finish:\n",
					Thread.currentThread().getName(),
					e);
		}
	}
}
