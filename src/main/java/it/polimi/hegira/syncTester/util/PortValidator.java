/**
 * 
 */
package it.polimi.hegira.syncTester.util;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

/**
 * @author marco
 *
 */
public class PortValidator implements IParameterValidator {

	@Override
	public void validate(String name, String value) throws ParameterException {
		try{
			int intValue = Integer.parseInt(value);
			if(intValue<1024 || intValue>65535)
				throw new ParameterException("Parameter "+name+" should be >1024 and <65535!");
		}catch(NumberFormatException e){
			throw new ParameterException("Parameter "+name+" should be >1024 and <65535!");
		}
	}

}
