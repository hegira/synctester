package it.polimi.hegira.syncTester.util;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class PositiveIntValidator implements IParameterValidator {

	@Override
	public void validate(String name, String value) throws ParameterException {
		try{
			int intValue = Integer.parseInt(value);
			if(intValue<=0){
				throw new ParameterException("Parameter "+name+" should be positive!");
			}
		}catch(NumberFormatException e){
			throw new ParameterException("Parameter "+name+" should be positive number!");
		}
	}

}
