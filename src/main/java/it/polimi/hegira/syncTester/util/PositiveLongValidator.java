/**
 * 
 */
package it.polimi.hegira.syncTester.util;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

/**
 * @author Marco Scavuzzo
 *
 */
public class PositiveLongValidator implements IParameterValidator {

	@Override
	public void validate(String name, String value) throws ParameterException {
		try{
			long longValue = Long.parseLong(value);
			if(longValue<0)
				throw new ParameterException("Parameter "+name+" should be positive!");
		}catch(NumberFormatException e){
			throw new ParameterException("Parameter "+name+" should be positive number!");
		}
	}
}
