package it.polimi.hegira.syncTester.coordination;

import it.polimi.hegira.syncTester.exceptions.InitializationException;
import it.polimi.hegira.zkWrapper.MigrationStatus;
import it.polimi.hegira.zkWrapper.VDPmigrationStatus;
import it.polimi.hegira.zkWrapper.ZKserver;
import it.polimi.hegira.zkWrapper.ZKserver2;
import it.polimi.hegira.zkWrapper.exception.OutOfSnapshotException;
import it.polimi.hegira.zkWrapper.statemachine.State;
import it.polimi.hegira.zkWrapper.statemachine.StateMachine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZooKeeper {
	private static final transient Logger log = LoggerFactory.getLogger(ZooKeeper.class);
	private static String connectString;
	private static ZKserver zk;
	private static ZKserver2 zk2;
	private static boolean useZkServer2 = false;
	private static Integer VDPsize=-1;
	
	private static ZooKeeper instance = new ZooKeeper();

	
	public ZooKeeper() {
	}
	
	/**
	 * Initializes {@link ZooKeeper} class by creating a stable connection
	 * to the given ZooKeeper connection string.
	 * @param cs ZooKeeper connection string ip:port
	 * @return The {@link ZooKeeper} class instance
	 */
	public static synchronized ZooKeeper init(String cs, int zkServerVersion){
		if(zkServerVersion>1){
			useZkServer2 = true;
		}
		if(connectString==null && cs!=null){
			connectString = cs;
			if((zk==null && !useZkServer2) || (zk2==null && useZkServer2))
				connect();
		}	
		return instance;
	}
	
	public static synchronized ZooKeeper reinit(String cs){
		if(connectString!=null){
			if(cs!=null){
				connectString = cs;
				if((zk!=null && !useZkServer2) || (zk2!=null && useZkServer2)){
					disconnect();
				}
				connect();
			}else{
				log.error("Provide a valid connection string");
			}
		}else{
			log.error("Never initilized");
		}
		return instance;
	}

	private static synchronized void connect(){
		if(connectString!=null && zk==null && !useZkServer2){
			log.debug("Connecting with ZKserver v1...");
			zk = new ZKserver(connectString);
		}else if(connectString!=null && zk2==null && useZkServer2){
			log.debug("Connecting with ZKserver v2...");
			zk2 = new ZKserver2(connectString);
		}
	}
	
	public static ZooKeeper getInstance() throws InitializationException{
		//if the user didn't initialize the class
		if(connectString==null)
			throw new InitializationException("Must first be initialized!");
		
		if(zk!=null || zk2!=null){
			//if already connected
			return instance;
		}else {
			//else if not connected, let's connect
			connect();
			return instance;
		}
	}
	
	public List<String> getTablesList(){
		List<String> tables = null;
		try {
			if(zk!=null && !useZkServer2)
				tables = zk.getMigrationPaths();
			else if(zk2!=null && useZkServer2)
				tables = zk2.getMigrationPaths();
			else
				log.error("Disconnected?");
		} catch (Exception e) {
			log.error("Couldn't get the list of tables! Have you started a migration?");
		}
		return tables!=null ? tables : new ArrayList<String>();
	}
	
	public static synchronized void disconnect(){
		if(zk!=null && !useZkServer2){
			zk.close();
			zk=null;
			log.debug("Disconnected");
		}else if(zk2!=null && useZkServer2){
			zk2.close();
			zk2=null;
			log.debug("Disconnected");
		}else{
			log.debug("Already disconnected");
		}
		
	}
	
	private MigrationStatus getTableMigrationStatus(String tableName){
		try {
			MigrationStatus ms = zk.getFreshMigrationStatus(tableName, null);
			return ms;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private VDPmigrationStatus getVDPmigrationStatus(String tableName, String vdpId){
		try{
			VDPmigrationStatus vms = zk2.getFreshMigrationStatus(tableName, vdpId, null);
			return vms;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public State getVDPstatus(String tableName, int VDPid) throws OutOfSnapshotException{
		if(tableName == null || VDPid < 0) return null;
		if(!useZkServer2){
			MigrationStatus ms = getTableMigrationStatus(tableName);
			StateMachine smStatus = ms.getVDPstatus(VDPid);
			State currentState = smStatus.getCurrentState();
			return currentState;
		}else{
			VDPmigrationStatus vdpMs = getVDPmigrationStatus(tableName, VDPid+"");
			if(vdpMs!=null)
				return vdpMs.getVDPstatus().getCurrentState();
		}
		return null;
	}
	
	public int getTableVDPsNo(String tableName){
		if(tableName == null) return 0;
		if(!useZkServer2){
			MigrationStatus ms = getTableMigrationStatus(tableName);
			return ms.getTotalVDPs(getVDPsize(false));
		}else{
			return getVDPmigrationStatus(tableName, "1").getTotalVDPs();
		}
	}
	
	public int getTableLastSeqNr(String tableName){
		if(tableName == null) return 0;
		if(!useZkServer2){
			MigrationStatus ms = getTableMigrationStatus(tableName);
			return ms.getLastSeqNr();
		}else{
			return getVDPmigrationStatus(tableName, "1").getLastSeqNr();
		}
	}
	
	/**
	 * Retrieves the size of all VDPs from ZooKeeper.
	 * I.e., 10^e
	 * @param force VDPsize cached value is possibly refreshed.
	 * @return The exponent e
	 */
	public int getVDPsize(boolean force){
		if(force || VDPsize<0){
			try {
				if(zk!=null && !zk.isLocked(null)){
					synchronized (VDPsize) {
						VDPsize = zk.getVDPsize();
					}
				}else if(zk2!=null && !zk2.isLocked(null)){
					synchronized(VDPsize){
						VDPsize = zk2.getVDPsize();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return VDPsize.intValue();
	}
}
