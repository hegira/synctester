/**
 * 
 */
package it.polimi.hegira.syncTester.schedulers;

import it.polimi.hegira.syncTester.eventSources.ScheduledStopSource;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class StopScheduler {
	private static Logger log = LoggerFactory.getLogger(StopScheduler.class);
	private final ScheduledExecutorService scheduler;
	private long delay = -1;
	private TimeUnit unit;
	private ScheduledFuture<?> schedule;
	private long issuedTime;
	
	/**
	 * Creates a new instance of the {@link StopScheduler} class 
	 * that stops the sync tester's threads (QueriesIssuerRunnable, VDPsCheckerRunnable) after a given delay.
	 * @param delay the time from now to delay execution
	 * @param unit the time unit of the delay parameter
	 */
	public StopScheduler(long delay, TimeUnit unit){
		this.scheduler = Executors.newScheduledThreadPool(1);
		this.delay = delay;
		this.unit = unit;
	}
	
	/**
	 * Creates and executes a one-shot stop action that becomes enabled after the given delay.
	 */
	public void scheduleStop(){
		if(scheduler == null || delay < 0 || unit == null){
			log.error("{} - StopScheduler class wasn't properly instantiated!", 
					Thread.currentThread().getName());
			return;
		}
		
		final Runnable stopRunnable = new Runnable() {
			@Override
			public void run() {
				log.info("{} - Stopping sync tester's threads",
						Thread.currentThread().getName());
				ScheduledStopSource.getInstance().notifyScheduledStop(delay, unit);
			}
		};
		
		log.info("{} - The component's threads will be stopped in {} {}",
						Thread.currentThread().getName(),
						delay, unit.name());
		schedule = scheduler.schedule(stopRunnable, delay, unit);
		this.issuedTime = System.currentTimeMillis();
	}
	
	/**
	 * Attempts to cancel execution of a stop task. This attempt will fail if the task has already completed, 
	 * has already been cancelled, or could not be cancelled for some other reason. 
	 * @return false if the task could not be cancelled, 
	 * typically because it has already completed normally (or it was not set); true otherwise
	 */
	public boolean cancelScheduledStop(){
		if(schedule!=null)
			return schedule.cancel(true);
		else
			return false;
	}

	/**
	 * @param delay the delay to set
	 */
	public synchronized void setDelay(long delay) {
		this.delay = delay;
	}

	/**
	 * @param unit the unit to set
	 */
	public synchronized void setUnit(TimeUnit unit) {
		this.unit = unit;
	}
	
	public String remainingTime(){
		long elapsed = System.currentTimeMillis() - issuedTime;
		if(unit.equals(TimeUnit.SECONDS)){
			elapsed = elapsed/1000;
		} else if(unit.equals(TimeUnit.MINUTES)){
			elapsed = (elapsed/1000)/60;
		} else if(unit.equals(TimeUnit.HOURS)){
			elapsed = (elapsed/1000)/3600;
		}
		return (delay-elapsed)+" "+unit.name();
	}
}
