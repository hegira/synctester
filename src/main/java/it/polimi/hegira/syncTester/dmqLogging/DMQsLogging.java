/**
 * 
 */
package it.polimi.hegira.syncTester.dmqLogging;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.kafkaProducer.ws.OperationResponse;
import it.polimi.hegira.syncTester.events.DMQresponseEvent;
import it.polimi.hegira.syncTester.events.DMQsResponseListener;

/**
 * @author Marco Scavuzzo
 *
 */
public class DMQsLogging implements DMQsResponseListener {
	private final transient Logger log = LoggerFactory.getLogger(DMQsLogging.class);
	private ExecutorService threadPool;
	
	private DMQsLogging(){
		threadPool = Executors.newCachedThreadPool();
		init();
	}
	
	private static class DMQsLoggingHolder{
		private static final DMQsLogging instance = new DMQsLogging();
	}
	
	public static DMQsLogging getInstance(){
		return DMQsLoggingHolder.instance;
	}
	
	private static void init(){
		try {
	        //Create logging.properties specified directory for logging in home directory
			String filePath = System.getProperty("user.home")+File.separator+
					"hegira"+File.separator+
					"syncTester"+File.separator
					+"hegira-DMQsResponses.log";
			File file = new File(filePath);
			if (!file.getParentFile().exists())
			    file.getParentFile().mkdirs();
			if (!file.exists())
			    file.createNewFile();
	    } catch(Exception e) {
	        e.printStackTrace();
	    }
	}
	
	@Override
	public void onDMQresponse(final DMQresponseEvent event) {
		if(event!=null && event.getResponse()!=null){
			threadPool.submit(new Runnable() {
				@Override
				public void run() {
					try{
						OperationResponse response;
						if(event.getDelay()>0 && event.getDelayUnit()!=null){
							response = event.getResponse().get(event.getDelay(), event.getDelayUnit());
						}else{
							response = event.getResponse().get();
						}
						log.info(response.toString());
					} catch (InterruptedException | ExecutionException | TimeoutException e) {
						log.error("No response received for {}",event.getResponse());
					}
				}
			});
		} else {
			if(event!=null){
				log.info("Got DMQresponseEvent from {}", event.getSource());
			}else{
				log.error("Received null event (DMQresponseEvent)!!");
			}
		}
	}
}
