/**
 * 
 */
package it.polimi.hegira.syncTester.eventSources;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.kafkaProducer.ws.OperationResponse;
import it.polimi.hegira.syncTester.events.DMQresponseEvent;
import it.polimi.hegira.syncTester.events.DMQsResponseListener;

/**
 * @author Marco Scavuzzo
 *
 */
public class DMQsResponseSource {
	private static final transient Logger log = LoggerFactory.getLogger(DMQsResponseSource.class);
	
	private ConcurrentLinkedQueue<DMQsResponseListener> listeners;
	
	/**
	 * Initialization-on-demand holder idiom
	 * https://en.wikipedia.org/wiki/Initialization-on-demand_holder_idiom
	 * Enables a safe, highly concurrent lazy initialization with good performance
	 */
	private static class DMQsResponseSourceHolder {
		private static final DMQsResponseSource instance = new DMQsResponseSource();
	}
	
	private DMQsResponseSource(){
		listeners = new ConcurrentLinkedQueue<DMQsResponseListener>();
	}
	
	public static DMQsResponseSource getInstance(){
		return DMQsResponseSourceHolder.instance;
	}
	
	public void addDMQsResponseListener(DMQsResponseListener listener){
		listeners.add(listener);
	}
	
	public void removeDMQsResponseListener(DMQsResponseListener listener){
		listeners.remove(listener);
	}
	
	public void notifyDMQsResponse(long delay, TimeUnit unit, Future<OperationResponse> response){
		DMQresponseEvent event = new DMQresponseEvent(this, delay, unit, response);
		for (DMQsResponseListener listener : listeners) {
			listener.onDMQresponse(event);
		}
	}
}
