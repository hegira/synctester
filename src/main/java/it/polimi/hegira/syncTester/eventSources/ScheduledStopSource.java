/**
 * 
 */
package it.polimi.hegira.syncTester.eventSources;

import it.polimi.hegira.syncTester.events.ScheduledStopEvent;
import it.polimi.hegira.syncTester.events.ScheduledStopListener;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class ScheduledStopSource {
	private static final transient Logger log = LoggerFactory.getLogger(ScheduledStopSource.class);
	private ConcurrentLinkedQueue<ScheduledStopListener> listeners;
	
	private final static ScheduledStopSource instance = new ScheduledStopSource();
	
	private ScheduledStopSource() {
		listeners = new ConcurrentLinkedQueue<ScheduledStopListener>();
	}
	
	public static ScheduledStopSource getInstance() {
		return instance;
	}
	
	public void addScheduledStopListener(ScheduledStopListener listener){
		listeners.add(listener);
	}
	
	public void removeScheduledStopListener(ScheduledStopListener listener){
		listeners.remove(listener);
	}
	
	public void notifyScheduledStop(long delay, TimeUnit unit){
		log.debug("{} - Notifying scheduled stop");
		ScheduledStopEvent event = new ScheduledStopEvent(this, delay, unit);
		
		while(listeners.isEmpty()){
			synchronized (listeners) {
				try {
					listeners.wait(500);
				} catch (InterruptedException e) {
					log.error("{} - Stack trace:\n",Thread.currentThread().getName(),e);
				}
			}
		}
		
		for(ScheduledStopListener listener : listeners){
			listener.onScheduledStop(event);
		}
	}
}
