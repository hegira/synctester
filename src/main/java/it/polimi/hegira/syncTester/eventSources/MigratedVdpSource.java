package it.polimi.hegira.syncTester.eventSources;

import it.polimi.hegira.syncTester.events.MigratedVdpEvent;
import it.polimi.hegira.syncTester.events.MigratedVdpListener;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class MigratedVdpSource {
	private static final transient Logger log = LoggerFactory.getLogger(MigratedVdpSource.class);
	private ConcurrentLinkedQueue<MigratedVdpListener> listeners;
	
	private final static MigratedVdpSource instance = new MigratedVdpSource();
	
	private MigratedVdpSource(){
		listeners = new ConcurrentLinkedQueue<MigratedVdpListener>();
	}
	
	public static MigratedVdpSource getInstance(){
		return instance;
	}
	
	public void addMigratedVdpListener(MigratedVdpListener listener){
		listeners.add(listener);
	}
	
	public void removeMigratedVdpListener(MigratedVdpListener listener){
		listeners.remove(listener);
	}
	
	public void notifyMigratedVdp(String tableName, Integer vdpId){
		log.debug("{} - Notifying subscribed listeners VDP {}/{} was migrated.",
				Thread.currentThread().getName(),
				tableName,vdpId);
		MigratedVdpEvent event = 
				new MigratedVdpEvent(this, tableName, vdpId, System.currentTimeMillis());
		while(listeners.isEmpty()){
			synchronized (listeners) {
				try {
					listeners.wait(500);
				} catch (InterruptedException e) {
					log.error("{} - Stack trace:\n",Thread.currentThread().getName(),e);
				}
			}
		}
		for(MigratedVdpListener listener : listeners){
			listener.onVdpMigrated(event);
		}
		event = null;
	}
}
