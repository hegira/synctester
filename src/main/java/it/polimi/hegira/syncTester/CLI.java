package it.polimi.hegira.syncTester;

import it.polimi.hegira.syncTester.util.PortValidator;
import it.polimi.hegira.syncTester.util.PositiveIntValidator;
import it.polimi.hegira.syncTester.util.PositiveLongValidator;
import it.polimi.hegira.syncTester.util.TimeUnitValidator;

import java.util.concurrent.TimeUnit;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

/**
 * @author Marco Scavuzzo
 *
 */
public class CLI {
	
	@Parameter(names = {"--zookeeper","-z"}, 
			description = "Zookeeper connect string (<ip>:<port>)",
			required = false, validateWith = ConnectStringValidator.class)
	public String zkConnectString = "localhost:2181";
	
	@Parameter(names = {"--zkServerVersion","-zv"}, 
			description = "Zookeeper Wrapper server version (version 2 increases VDP lock granularity)",
			required = false, validateWith = PositiveIntValidator.class)
	public int zkServerVersion = 1;
	
	@Parameter(names = {"--kafka","-k"}, 
			description = "Kafka connect string (<ip>:<port>)",
			required = false, validateWith = ConnectStringValidator.class)
	public String kafkaConnectString = "localhost:9092";
	
	@Parameter(names = {"--issuers", "-i"},
			description = "The number of threads used to issue DMQs",
			required = false)
	public int queriesIssuers = 1;
	
	@Parameter(names = {"--topic","-t"},
			description = "Synchronization topic name",
			required = false)
	public String clTopic = "synch";
	
	@Parameter(names = {"--epvdp","-e"},
			description = "The percentage of entities to generate per each VDP w.r.t. the VDPs' size",
			required = false)
	public float entitiesPerVDPperc = 10.0f; 
	
	@Parameter(names = {"--vdppt","-v"},
			description = "The percentage of VDPs to consider to generate the entities w.r.t. the total number of VDPs per table",
			required = false)
	public float vdpsPerTableperc = 30.0f;
	
	@Parameter(names = {"--seed","-s"}, 
			description = "The initial value of the internal state of the pseudorandom number generator, "
					+ "which is used to generated VDP ids and entities ids. "
					+ "If not specified, no seed will be used.",
					required = false)
	public Long seed;
	
	@Parameter(names = {"--pause","-p"}, 
			description = "The time, in milliseconds, to wait between a query and another. "
					+ "If not set there's no wait time between the queries; "
					+ "just a random wait time (>=500ms & <=5s), if setted, is applied between queries-sets over different VDPs",
					required = false)
	public long intraQueriesPause = -1;
	
	@Parameter(names = {"--tweet","-tw"}, 
			description = "If set to true it generates DMQs in the form of real tweets. "
					+ "If set to false it generates DMQs in the form of random Key Value pairs.",
					required = false)
	public boolean generateTweets = false;
	
	@Parameter(names = {"--setpause","-sp"}, 
			description = "A random wait time (>=500ms & <= <value>) is applied between query-sets over different VDPs."
					+ "It actually allows for random workloads!",
					required = false)
	public long interQuerySetsPause = 0;
	
	@Parameter(names = {"--debug","-dbg"}, 
			description = "If set to true it shows debug messages. ",
					required = false)
	public boolean debug = false;
	
	@Parameter(names = {"--help","-h"}, help = true,
			description = "Prints this help screen")
    public boolean help = false;
	
	@Parameter(names = {"--del","-d"}, 
			description = "If set to true it also generates delete operations, otherwise it just generates inserts and updates.",
					required = false)
	public boolean deletes = false;
	
	@Parameter(names = {"--os","-o"}, 
			description = "If set to true it may generate operations out of the snapshot, otherwise, until the current data migration isn't complete, it just generates operations within the snapshot.",
					required = false)
	public boolean outSnapshot = false;

	@Parameter(names = {"--is"}, 
			description = "If set to false it does not generate operations inside the snapshot. "
					+ "If only operations out of the snapshot are generated they are guaranteed to be unique!!",
					required = false, arity = 1)
	public boolean insideSnapshot=true;
	
	
	@Parameters(commandDescription = "Schedules a task that will stop the synctester after a given amount of time "
			+ "without waiting, possibly, for the data migration to be completed; i.e., queries out of the snapshot won't be performed")
	public class StopCommand {
		public static final String commandName = "stop";
		
		@Parameter(names = { "--sDelay","-d" }, 
				description = "The time from now to delay execution of the task that will stop the component's threads (data generators).",
				validateWith=PositiveLongValidator.class,
				required=true)
		public long stopDelay = -1;
		
		@Parameter(names = {"--unit", "-u"},
				description = "The time unit of the delay parameter.",
				validateWith = TimeUnitValidator.class)
		public String stopDelayUnit = TimeUnit.MINUTES.name();
		
		public TimeUnit parseTimeUnit(String unit){
			if(unit.equals(TimeUnit.SECONDS.name())){
				return TimeUnit.SECONDS;
			}else if(unit.equals(TimeUnit.MINUTES.name())){
				return TimeUnit.MINUTES;
			}else if(unit.equals(TimeUnit.HOURS.name())){
				return TimeUnit.HOURS;
			}else{
				return null;
			}
		}
	}
	
	@Parameters(commandDescription = "Ask for positive/negative acknoweledgment to generated DMQs")
	public class DMQsResponseCommand {
		public static final String commandName = "DMQsResponse";
		
		@Parameter(names = {"--log", "-l"},
				description = "Logs the response to DMQs and stores them in a file (in the user home, under the folder hegira/syncTester).")
		public boolean logging = true;
		
		@Parameter(names = {"--waitTime", "-w"},
				description = "The time to wait for receiving DMQs response (per query). If negative waits indefinitely.")
		public long waitTime = -1;
		
		@Parameter(names = {"--unit", "-u"},
				description = "The time unit of the waitTime parameter.",
				validateWith = TimeUnitValidator.class)
		public String waitTimeUnit = TimeUnit.SECONDS.name();
		
		@Parameter(names = {"--port", "-p"},
				description = "The port of the Web Server listening for DMQs responses.",
				validateWith = PortValidator.class)
		public int port = 6066;
		
		@Parameter(names = {"--discovery","-d"}, 
				description = "Discovery service connect string (<ip>:<port>)",
				required = false, validateWith = ConnectStringValidator.class)
		public String dConnectString = "localhost:2181";
		
		public TimeUnit parseTimeUnit(String unit){
			if(unit.equals(TimeUnit.SECONDS.name())){
				return TimeUnit.SECONDS;
			}else if(unit.equals(TimeUnit.MINUTES.name())){
				return TimeUnit.MINUTES;
			}else if(unit.equals(TimeUnit.HOURS.name())){
				return TimeUnit.HOURS;
			}else{
				return null;
			}
		}
	}
	
	@Parameters(commandDescription = "Generate groups of atomic operations.")
	public class GroupOperations {
		public static final String commandName = "GroupOperations";
		
		@Parameter(names = {"--grouSize", "-g"},
				description = "The number of operations to be assigned to each group.",
				validateWith = PositiveIntValidator.class,
				required=true)
		public int groupSize = 5;
	}
}
