package it.polimi.hegira.syncTester.exceptions;

/**
 * @author Marco Scavuzzo
 *
 */
public class InitializationException extends Exception {

	public InitializationException(String string) {
		super(string);
	}

}
