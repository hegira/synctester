package it.polimi.hegira.syncTester.queries;

import static org.junit.Assert.*;
import it.polimi.hegira.syncTester.queries.QueriesIssuerRunnable;

import org.junit.Test;

public class DateGenerationTest {

	@Test
	public void testDateGeneration() {
		int i=0;
		printType("Ciao"); printType(i);
		for(i=0; i<10; i++){
			long time = System.currentTimeMillis();
			System.out.println(time+" --> "+QueriesIssuerRunnable.millsToDateString(time));
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				fail("Test failed");
			}
		}
		assertTrue(i==10);
	}
	
	private void printType(Object o){
		System.out.println(o.getClass().getSimpleName());
	}

}
