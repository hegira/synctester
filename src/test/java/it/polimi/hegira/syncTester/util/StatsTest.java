/**
 * 
 */
package it.polimi.hegira.syncTester.util;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author marco
 *
 */
public class StatsTest {

	/**
	 * Test method for {@link it.polimi.hegira.syncTester.util.Stats#getTotalOperations()}.
	 * @throws InterruptedException 
	 */
	@Test
	public void testGetTotalOperations() throws InterruptedException {
		Stats stats = Stats.getInstance();
		System.out.println(stats);
		stats.incrementDeletes(false);
		stats.incrementInserts(true);
		for(int i=0; i<100; i++){
			stats.incrementUpdates(false);
		}
		final long total = 102;
		System.out.println(stats);
		
		stats.stopWriter();
		assertEquals(total, stats.getTotalOperations());
	}

}
