package it.polimi.hegira.syncTester.coordination;

import static org.junit.Assert.*;

import java.util.List;

import it.polimi.hegira.syncTester.exceptions.InitializationException;

import org.junit.Test;
import org.junit.internal.runners.statements.Fail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZooKeeperTest {
	Logger log = LoggerFactory.getLogger(ZooKeeperTest.class);
	
	public ZooKeeperTest() {
	}

	@Test
	public void testCreation(){
		try{
			ZooKeeper zk = ZooKeeper.getInstance();
		}catch(InitializationException e){
			log.info("Threw a null pointer exception");
		}
		
		try {
			ZooKeeper zk = ZooKeeper.init("localhost:2181",1).getInstance();
			List<String> tablesList = zk.getTablesList();
			if(tablesList!=null){
				for(String table : tablesList){
					log.info(table);
				}
			}
			assertTrue(true);
		} catch (InitializationException e) {
			fail();
		}finally{
			ZooKeeper.disconnect();
			try {
				ZooKeeper.getInstance();
			} catch (InitializationException e) {
				log.error("InitializationException: ",e);
				e.printStackTrace();
			} catch(Exception e){
				log.error("Other Exception: ",e);
				e.printStackTrace();
			}
		}
		
	}
	
	@Test
	public void testGetVDPsize(){
		try {
			log.info("VDPsize: {}", ZooKeeper.getInstance().getVDPsize(false));
		} catch (InitializationException e) {
			e.printStackTrace();
		} finally {
			ZooKeeper.disconnect();
		}
	}
}
