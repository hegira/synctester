# README #


### Setup DMQs responses logging ###

In order to capture the log of DMQs responses configure the log4j.properties file as follows, and place it in folder *${user.home}/hegira/syncTester*


```
#!java

#log4j.properties
#LOGGER
log4j.logger.it.polimi=DEBUG
log4j.logger.it.polimi.hegira.syncTester.dmqLogging=DEBUG, DMQ
log4j.rootLogger=ERROR, stdout
# =============== console output appender =====================
log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=[%d{HH:mm:ss}] %5p: (%F:%L) - %m%n

# =============== File output appender =====================
log4j.appender.DMQ=org.apache.log4j.RollingFileAppender
log4j.appender.DMQ.File=${user.home}/hegira/syncTester/hegira-DMQsResponses.log
log4j.appender.DMQ.layout=org.apache.log4j.PatternLayout
log4j.appender.DMQ.layout.ConversionPattern=[%d{HH:mm:ss}] %5p: (%F:%L) - %m%n
log4j.additivity.it.polimi.hegira.syncTester.dmqLogging=false
```